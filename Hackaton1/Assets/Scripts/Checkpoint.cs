﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Checkpoint : MonoBehaviour
{
    public bool EndGameTrigger;
    public bool NextLevel;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag=="Player")
        {
            FindObjectOfType<Player>();

            if (EndGameTrigger)
            {
                SceneManager.LoadScene("Main Menu");
            }
            else if (NextLevel)
            {
                SceneManager.LoadScene("Game2");
            }
            else
            {
                other.GetComponent<Player>().SetRespawnPoint(transform);
            }

        }
    }
}
