﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    Rigidbody2D rigidbody;
    public int jumpPower;
    public int walkSpeed;
    public GameObject SpawnPoint;
    SpriteRenderer mySpriteRenderer;
    public Text LevelNumber;
    public Text LivesNumber;
    public Text PointsNumber;
    public AudioSource JumpSound;

    // Start is called before the first frame update
    void Start()
    {
        rigidbody = GetComponent<Rigidbody2D>();
        mySpriteRenderer = GetComponent<SpriteRenderer>();
        LevelNumber.text = "1";
        LivesNumber.text = "1";
        PointsNumber.text = "0";
    }

    // Update is called once per frame
    void Update()
    {
        float movement = Input.GetAxis("Horizontal");

        transform.Translate(Vector3.right * movement * Time.deltaTime * walkSpeed);

        if (movement < 0)
        {
            mySpriteRenderer.flipX = true;
        }
        else if (movement > 0)
        {
            mySpriteRenderer.flipX = false;
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            rigidbody.AddRelativeForce(Vector2.up * jumpPower, ForceMode2D.Impulse);
            JumpSound.Play();
        }
    }

    public void SetRespawnPoint(Transform respawnPoint)
    {
        SpawnPoint.transform.position = respawnPoint.position;
    }

    public void Respawn()
    {
        transform.position = SpawnPoint.transform.position;
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Enemy")
        {
            Respawn();
            //Destroy(other);
            //SceneManager.LoadScene("MainMenu");

        }
    }
}
