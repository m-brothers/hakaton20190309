﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{
    public void StartLevel1()
    {
        SceneManager.LoadScene("Game1");
    }
    public void StartLevel2()
    {
        SceneManager.LoadScene("Game2");
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
