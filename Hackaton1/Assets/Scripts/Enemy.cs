﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Enemy : MonoBehaviour
{
    public float Speed = 1f;
    public float TurnTime = 2f;

    private Vector2 Direction;

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            Destroy(gameObject);
            Debug.Log("hit");

        }
    }
    private void Start()
    {
        Direction = Vector2.right;
        StartCoroutine(ChangeDirection());
    }
    private void Update()
    {
        transform.Translate(Direction * Time.deltaTime * Speed);
    }
    IEnumerator ChangeDirection()
    {
        while (true)
        {
            yield return new WaitForSeconds(TurnTime);
            Direction = Vector2.left;
            yield return new WaitForSeconds(TurnTime);
            Direction = Vector2.right;
        }
    }
}
